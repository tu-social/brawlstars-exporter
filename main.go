package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var logger = log.WithPrefix(
	log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr)),
	"time", log.DefaultTimestampUTC,
	"caller", log.DefaultCaller,
)

var alwaysUpdatePlayers = os.Getenv("ALWAYS_UPDATE_PLAYERS") == "true"
var updateInterval = 5 * time.Minute

func main() {
	apiKey := os.Getenv("API_KEY")
	if apiKey == "" {
		level.Error(logger).Log("msg", "API_KEY environment variable is not set")
		os.Exit(1)
	}

	clubTag := os.Getenv("CLUB_TAG")
	if clubTag == "" {
		level.Error(logger).Log("msg", "CLUB_TAG environment variable is not set")
		os.Exit(1)
	}

	updateIntervalEnv := os.Getenv("UPDATE_INTERVAL")
	if updateIntervalEnv != "" {
		var err error
		updateInterval, err = time.ParseDuration(updateIntervalEnv)
		if err != nil {
			level.Error(logger).Log("msg", "failed to parse UPDATE_INTERVAL", "err", err)
			os.Exit(1)
		}
	}

	reg := prometheus.NewRegistry()
	httpRequestsDuration := promauto.With(reg).NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_request_duration_seconds",
	}, []string{"method", "path", "status_code"})
	client := &http.Client{
		Transport: roundTripperFunc(func(req *http.Request) (resp *http.Response, err error) {
			// This will leak path's over time as players leave and join. I don't care.
			defer func(t0 time.Time) {
				if err != nil {
					httpRequestsDuration.WithLabelValues(req.Method, req.URL.Path, "error").Observe(time.Since(t0).Seconds())
				} else {
					httpRequestsDuration.WithLabelValues(req.Method, req.URL.Path, strconv.Itoa(resp.StatusCode)).Observe(time.Since(t0).Seconds())
				}
			}(time.Now())
			req.Header.Set("Authorization", "Bearer "+apiKey)
			return http.DefaultTransport.RoundTrip(req)
		}),
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	info, err := getClubInfo(ctx, client, clubTag)
	if err != nil {
		level.Error(logger).Log("msg", "failed to get club info for the first time", "err", err)
		os.Exit(1)
	}

	brawlStarsRegistry := prometheus.NewRegistry()
	clubGauge := func(name string) *prometheus.GaugeVec {
		return promauto.With(brawlStarsRegistry).NewGaugeVec(prometheus.GaugeOpts{
			Name: name,
		}, []string{"club_name", "club_tag"})
	}
	playerGauge := func(name string) *prometheus.GaugeVec {
		return promauto.With(brawlStarsRegistry).NewGaugeVec(prometheus.GaugeOpts{
			Name: name,
		}, []string{"club_name", "club_tag", "player_name", "player_tag"})
	}
	brawlerGauge := func(name string) *prometheus.GaugeVec {
		return promauto.With(brawlStarsRegistry).NewGaugeVec(prometheus.GaugeOpts{
			Name: name,
		}, []string{"club_name", "club_tag", "player_name", "player_tag", "brawler_name", "brawler_id"})
	}
	metrics := metrics{
		club: clubMetrics{
			members:  clubGauge("brawlstars_club_members"),
			trophies: clubGauge("brawlstars_club_trophies"),
		},
		players: playersMetrics{
			trophies:              playerGauge("brawlstars_player_trophies"),
			highestTrophies:       playerGauge("brawlstars_player_highest_trophies"),
			expLevel:              playerGauge("brawlstars_player_exp_level"),
			expPoints:             playerGauge("brawlstars_player_exp_points"),
			threeVsThreeVictories: playerGauge("brawlstars_player_3vs3_victories"),
			soloVictories:         playerGauge("brawlstars_player_solo_victories"),
			duoVictories:          playerGauge("brawlstars_player_duo_victories"),

			brawlerPower:           brawlerGauge("brawlstars_brawler_power"),
			brawlerRank:            brawlerGauge("brawlstars_brawler_rank"),
			brawlerTrophies:        brawlerGauge("brawlstars_brawler_trophies"),
			brawlerHighestTrophies: brawlerGauge("brawlstars_brawler_highest_trophies"),
		},
	}

	// Run a first update.
	clubMembers := map[string]clubMember{}
	processClubInfo(ctx, client, info, clubMembers, metrics)

	// Also update periodically.
	go func() {
		ticker := time.NewTicker(updateInterval)
		defer ticker.Stop()
		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				info, err := getClubInfo(ctx, client, clubTag)
				if err != nil {
					level.Error(logger).Log("msg", "failed to get club info", "err", err)
					continue
				}
				processClubInfo(ctx, client, info, clubMembers, metrics)
			}
		}
	}()

	observabilityServerListener, err := net.Listen("tcp", ":8080")
	if err != nil {
		level.Error(logger).Log("msg", "failed to listen on :8080", "err", err)
		os.Exit(1)
	}
	defer observabilityServerListener.Close()
	level.Info(logger).Log("msg", "listening on :8080")
	{
		mux := http.NewServeMux()
		mux.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
		go func() {
			srv := &http.Server{
				BaseContext: func(listener net.Listener) context.Context { return ctx },
				Handler:     mux,
			}
			if err := srv.Serve(observabilityServerListener); err != nil {
				level.Error(logger).Log("msg", "failed to serve", "err", err)
				os.Exit(1)
			}
		}()
	}

	brawlStarsServerListener, err := net.Listen("tcp", ":8081")
	if err != nil {
		level.Error(logger).Log("msg", "failed to listen on :8081", "err", err)
		os.Exit(1)
	}
	defer brawlStarsServerListener.Close()
	level.Info(logger).Log("msg", "listening on :8081")
	{
		mux := http.NewServeMux()
		mux.Handle("/metrics", promhttp.HandlerFor(brawlStarsRegistry, promhttp.HandlerOpts{}))
		go func() {
			srv := &http.Server{
				BaseContext: func(listener net.Listener) context.Context { return ctx },
				Handler:     mux,
			}
			if err := srv.Serve(brawlStarsServerListener); err != nil {
				level.Error(logger).Log("msg", "failed to serve", "err", err)
				os.Exit(1)
			}
		}()
	}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)
	<-sig

	level.Info(logger).Log("msg", "shutting down")
	cancel()

	level.Info(logger).Log("msg", "waiting a second because I'm too lazy to add a waitgroup here. anyway, nobody cares.")
	<-time.After(time.Second)
}

type metrics struct {
	club    clubMetrics
	players playersMetrics
}

type clubMetrics struct {
	members  *prometheus.GaugeVec
	trophies *prometheus.GaugeVec
}

func processClubInfo(ctx context.Context, client *http.Client, info club, clubMembers map[string]clubMember, metrics metrics) {
	clubLabels := prometheus.Labels{"club_name": info.Name, "club_tag": info.Tag}
	metrics.club.members.With(clubLabels).Set(float64(len(info.Members)))
	metrics.club.trophies.With(clubLabels).Set(float64(info.Trophies))

	seen := map[string]struct{}{}
	for _, member := range info.Members {
		seen[member.Tag] = struct{}{}
		if cm, ok := clubMembers[member.Tag]; !ok {
			level.Info(logger).Log("msg", "new club member", "name", member.Name, "tag", member.Tag, "trophies", member.Trophies)
			if updatePlayer(ctx, client, info, member.Tag, metrics.players) {
				// only update on successful player update, so we can retry on failure.
				clubMembers[member.Tag] = member
			}
		} else if cm.Trophies != member.Trophies {
			level.Info(logger).Log("msg", "club member trophies updated", "name", member.Name, "tag", member.Tag, "trophies", member.Trophies)
			if updatePlayer(ctx, client, info, member.Tag, metrics.players) {
				// only update on successful player update, so we can retry on failure.
				clubMembers[member.Tag] = member
			}
		} else if alwaysUpdatePlayers {
			if updatePlayer(ctx, client, info, member.Tag, metrics.players) {
				// only update on successful player update, so we can retry on failure.
				clubMembers[member.Tag] = member
			}
		} else {
			level.Debug(logger).Log("msg", "club member trophies not updated", "name", member.Name, "tag", member.Tag, "trophies", member.Trophies)
		}
		// Distribute the requests over the update interval, so we don't hit the API at the same time.
		time.Sleep(updateInterval / time.Duration(len(info.Members)))
	}
	for _, member := range clubMembers {
		if _, ok := seen[member.Tag]; !ok {
			level.Info(logger).Log("msg", "club member left", "name", member.Name, "tag", member.Tag, "trophies", member.Trophies)
			delete(clubMembers, member.Tag)
			playerLabels := prometheus.Labels{"player_tag": member.Tag}
			metrics.players.trophies.DeletePartialMatch(playerLabels)
			metrics.players.highestTrophies.DeletePartialMatch(playerLabels)
			metrics.players.expLevel.DeletePartialMatch(playerLabels)
			metrics.players.expPoints.DeletePartialMatch(playerLabels)
			metrics.players.threeVsThreeVictories.DeletePartialMatch(playerLabels)
			metrics.players.soloVictories.DeletePartialMatch(playerLabels)
			metrics.players.duoVictories.DeletePartialMatch(playerLabels)
			metrics.players.brawlerPower.DeletePartialMatch(playerLabels)
			metrics.players.brawlerRank.DeletePartialMatch(playerLabels)
			metrics.players.brawlerTrophies.DeletePartialMatch(playerLabels)
			metrics.players.brawlerHighestTrophies.DeletePartialMatch(playerLabels)
		}

	}
	level.Info(logger).Log("msg", "club info processed")
}

func updatePlayer(ctx context.Context, client *http.Client, clubInfo club, tag string, m playersMetrics) bool {
	info, err := getPlayerInfo(ctx, client, tag)
	if err != nil {
		return false
	}

	playerLabels := prometheus.Labels{
		"club_name":   clubInfo.Name,
		"club_tag":    clubInfo.Tag,
		"player_name": info.Name,
		"player_tag":  info.Tag,
	}
	m.trophies.With(playerLabels).Set(float64(info.Trophies))
	m.highestTrophies.With(playerLabels).Set(float64(info.HighestTrophies))
	m.expLevel.With(playerLabels).Set(float64(info.ExpLevel))
	m.expPoints.With(playerLabels).Set(float64(info.ExpPoints))
	m.threeVsThreeVictories.With(playerLabels).Set(float64(info.ThreeVsThreeVictories))
	m.soloVictories.With(playerLabels).Set(float64(info.SoloVictories))
	m.duoVictories.With(playerLabels).Set(float64(info.DuoVictories))

	for _, b := range info.Brawlers {
		brawlerLabels := prometheus.Labels{
			"club_name":    clubInfo.Name,
			"club_tag":     clubInfo.Tag,
			"player_name":  info.Name,
			"player_tag":   info.Tag,
			"brawler_name": b.Name,
			"brawler_id":   strconv.Itoa(b.Id),
		}
		m.brawlerPower.With(brawlerLabels).Set(float64(b.Power))
		m.brawlerRank.With(brawlerLabels).Set(float64(b.Rank))
		m.brawlerTrophies.With(brawlerLabels).Set(float64(b.Trophies))
		m.brawlerHighestTrophies.With(brawlerLabels).Set(float64(b.HighestTrophies))
	}
	return true
}

func getClubInfo(ctx context.Context, client *http.Client, clubTag string) (club, error) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	u := fmt.Sprintf("https://api.brawlstars.com/v1/clubs/%s", url.QueryEscape(clubTag))
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u, nil)
	if err != nil {
		return club{}, fmt.Errorf("can't build req: %w", err)
	}
	resp, err := client.Do(req)
	if err != nil {
		level.Error(logger).Log("msg", "failed to get club info", "err", err)
		return club{}, fmt.Errorf("failed to perform club request: %w", err)
	}
	var info club
	if err := readJSONResp(resp, &info); err != nil {
		level.Error(logger).Log("msg", "failed to read club info", "err", err)
		return club{}, fmt.Errorf("can't read response: %w", err)
	}
	level.Info(logger).Log("msg", "successfully got club info", "name", info.Name, "tag", info.Tag, "members", len(info.Members))
	return info, err
}

func getPlayerInfo(ctx context.Context, client *http.Client, tag string) (player, error) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	u := fmt.Sprintf("https://api.brawlstars.com/v1/players/%s", url.QueryEscape(tag))
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u, nil)
	if err != nil {
		level.Error(logger).Log("msg", "can't build req", "err", err)
		return player{}, err
	}
	resp, err := client.Do(req)
	if err != nil {
		level.Error(logger).Log("msg", "failed to get player info", "err", err)
		return player{}, err
	}

	var info player
	if err := readJSONResp(resp, &info); err != nil {
		level.Error(logger).Log("msg", "failed to read player info", "err", err)
		return player{}, err
	}

	level.Info(logger).Log("msg", "successfully got player info", "name", info.Name, "tag", info.Tag, "trophies", info.Trophies)
	return info, err
}

func readJSONResp(resp *http.Response, m any) error {
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}
	return json.NewDecoder(resp.Body).Decode(m)
}

type roundTripperFunc func(req *http.Request) (*http.Response, error)

func (r roundTripperFunc) RoundTrip(request *http.Request) (*http.Response, error) { return r(request) }

type club struct {
	Tag              string       `json:"tag"`
	Name             string       `json:"name"`
	Description      string       `json:"description"`
	Type             string       `json:"type"`
	BadgeId          int          `json:"badgeId"`
	RequiredTrophies int          `json:"requiredTrophies"`
	Trophies         int          `json:"trophies"`
	Members          []clubMember `json:"members"`
}

type clubMember struct {
	Tag       string `json:"tag"`
	Name      string `json:"name"`
	NameColor string `json:"nameColor"`
	Role      string `json:"role"`
	Trophies  int    `json:"trophies"`
}

type playersMetrics struct {
	trophies              *prometheus.GaugeVec
	highestTrophies       *prometheus.GaugeVec
	expLevel              *prometheus.GaugeVec
	expPoints             *prometheus.GaugeVec
	threeVsThreeVictories *prometheus.GaugeVec
	soloVictories         *prometheus.GaugeVec
	duoVictories          *prometheus.GaugeVec

	brawlerPower           *prometheus.GaugeVec
	brawlerRank            *prometheus.GaugeVec
	brawlerTrophies        *prometheus.GaugeVec
	brawlerHighestTrophies *prometheus.GaugeVec
}

type player struct {
	Tag                                  string    `json:"tag"`
	Name                                 string    `json:"name"`
	NameColor                            string    `json:"nameColor"`
	Trophies                             int       `json:"trophies"`
	HighestTrophies                      int       `json:"highestTrophies"`
	ExpLevel                             int       `json:"expLevel"`
	ExpPoints                            int       `json:"expPoints"`
	IsQualifiedFromChampionshipChallenge bool      `json:"isQualifiedFromChampionshipChallenge"`
	ThreeVsThreeVictories                int       `json:"3vs3Victories"`
	SoloVictories                        int       `json:"soloVictories"`
	DuoVictories                         int       `json:"duoVictories"`
	BestRoboRumbleTime                   int       `json:"bestRoboRumbleTime"`
	BestTimeAsBigBrawler                 int       `json:"bestTimeAsBigBrawler"`
	Club                                 club      `json:"club"` // only tag and name
	Brawlers                             []brawler `json:"brawlers"`
}

type brawler struct {
	Id              int                `json:"id"`
	Name            string             `json:"name"`
	Power           int                `json:"power"`
	Rank            int                `json:"rank"`
	Trophies        int                `json:"trophies"`
	HighestTrophies int                `json:"highestTrophies"`
	Gears           []brawlerGear      `json:"gears"`
	StarPowers      []brawlerStarPower `json:"starPowers"`
	Gadgets         []brawlerGadget    `json:"gadgets"`
}

type brawlerGear struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Level int    `json:"level"`
}

type brawlerStarPower struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type brawlerGadget struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}
